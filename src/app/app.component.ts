import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ReportPage } from "../pages/report/report";

import { UniqueDeviceID } from "@ionic-native/unique-device-id";
import { Storage } from "@ionic/storage";
import { Geolocation } from "@ionic-native/geolocation";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform,
              public statusBar: StatusBar,
              public splashScreen: SplashScreen,
              public uniqueDeviceId: UniqueDeviceID,
              public storage: Storage,
              public geolocation: Geolocation) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: '위험물 지도', component: HomePage },
      { title: '신고하기', component: ReportPage}
    ];

    this.uniqueDeviceId.get().then((uuid) => {
      console.log(uuid);
      this.storage.set("uuid", uuid.substring(0, 7));
    }).catch((error) => {
      console.log(error);
      this.storage.set("uuid", "Kocc23Sk")
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the contents nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
