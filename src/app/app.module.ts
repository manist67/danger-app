import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { DetailPage } from "../pages/detail/detail";
import { ReportPage } from "../pages/report/report";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Geolocation } from "@ionic-native/geolocation";
import { IonicStorageModule } from "@ionic/storage";
import { UniqueDeviceID } from "@ionic-native/unique-device-id";

import { LoadingComponent } from '../components/loading/loading';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    DetailPage,
    ReportPage,
    LoadingComponent
  ],
  imports: [
    BrowserModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    DetailPage,
    ReportPage,
    LoadingComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    UniqueDeviceID,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
