import { Component, Output, EventEmitter } from '@angular/core';

import { Storage } from "@ionic/storage";
import { ToastController } from "ionic-angular";

import { Pos } from "../../models/pos";
import { Comments } from "../../models/comments";

/**
 * Generated class for the DetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-detail',
  templateUrl: 'detail.html',
})
export class DetailPage {
  @Output() closeDetail: EventEmitter<any> = new EventEmitter<any>();

  pos: Pos;
  comments: Array<Comments>;
  inputComment: string;

  constructor(private storage: Storage,
              private toastCtrl: ToastController) {
  }

  select(pos: Pos) {
    this.pos = pos;
    this.getPosDetailInfo();
    this.getPosComments(this.pos.seq);
  }

  getPosDetailInfo() {
    //TODO: todo
  }

  getPosComments(seq: number) {
    this.comments = [
    ];
  }

  sendComment(contents: string) {
    if(contents) {
      this.storage.get("uuid").then(value => {
        console.log(value);
        if(!value) {
          this.showToast("어플리케이션에 오류가 발생했습니다. 재실행해주세요.");
          return;
        }
        //TODO: http connection
        const comment = {
          id: value ,
          contents: contents,
          date: new Date()
        };

        this.comments.push(comment);
      });
    }
  }

  emitClose() {
    this.closeDetail.emit(null);
  }

  showToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'top'
    });

    toast.present();
  }
}
