import { Component, ViewChild } from '@angular/core';
import {NavController, Platform, ToastController} from 'ionic-angular';
import { Pos } from "../../models/pos";
import { DetailPage } from "../detail/detail";

import { Geolocation } from "@ionic-native/geolocation";
import { Storage } from "@ionic/storage";

declare var naver: any; // for naver map application

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  position: string;
  map: any;

  @ViewChild(DetailPage) detailPage:DetailPage;
  open: boolean = false;

  posList: Array<Pos> = [];
  selectedPos: Pos;
  markerList: Array<any> = [];

  currentMarker: any = undefined;

  tracker: any;

  bPositionLoading: boolean = false;

  constructor(public navCtrl: NavController,
              public platform: Platform,
              public geolocation: Geolocation,
              public toastCtrl: ToastController,
              public storage: Storage) {
    this.storage.set("posList", [{
      seq: 1,
      latitude: 37.5574771,
      longitude: 126.9998631,
      likeCount: 0,
      address: "서울특별시 중구 장충동 필동로1길 30",
      contents: "쓰레기들이 널부러져 있습니다.",
      image : "http://ec2-54-180-102-178.ap-northeast-2.compute.amazonaws.com/images/1.jpg",
      category: "환경미화"
    }, {
      seq: 2,
      latitude: 37.560355,
      longitude: 126.9947091,
      likeCount: 0,
      address: "서울특별시 중구 필동 서애로 26",
      contents: "노약자 배려시설은 언덕이 높아 노인분들이 걷기 힘들것 같다 계단을 설치하면 좋을 것 같다",
      image : "http://ec2-54-180-102-178.ap-northeast-2.compute.amazonaws.com/images/3.jpg",
      category: "노약자 배려시설"
    }, {
      seq: 3,
      latitude: 37.5611999,
      longitude: 126.997076,
      likeCount: 5,
      address: "서울특별시 중구 필동2가 60-1",
      contents: "전깃줄이 끊어져 내려와있다. 안전을 위해 정리해주세요.",
      image : "http://ec2-54-180-102-178.ap-northeast-2.compute.amazonaws.com/images/4.jpg",
      category: "공공시설수리"
    }, {
      seq: 4,
      latitude: 37.561612,
      longitude: 126.9959843,
      likeCount: 4,
      address: "서울 중구 서애로 6-1 인산빌딩",
      image : "http://ec2-54-180-102-178.ap-northeast-2.compute.amazonaws.com/images/2.jpg",
      category: "기타"
    }]);
  }

  ionViewWillEnter() {
    this.map = new naver.maps.Map('map',{
      center: new naver.maps.LatLng(37.5606715,126.9954462)
    });

    this.getPosList();

    this.tracker = this.geolocation.watchPosition().subscribe(position => {
      if(this.currentMarker == undefined) {
        this.currentMarker = new naver.maps.Marker({
          position: new naver.maps.LatLng(position.coords.latitude, position.coords.longitude),
          map: this.map,
          icon: {
            url: 'http://ec2-54-180-102-178.ap-northeast-2.compute.amazonaws.com/images/myposition.png',
            size: new naver.maps.Size(15, 15),
            scaledSize: new naver.maps.Size(15, 15),
            origin: new naver.maps.Point(0, 0),
            anchor: new naver.maps.Point(0, 0)
          }
        });
      } else {
        this.currentMarker.setPosition(new naver.maps.LatLng(position.coords.latitude, position.coords.longitude));
      }
    });
  }

  ionViewWillLeave() {
    this.storage.set("posList", this.posList);
    //TODO: remove

    this.tracker.unsubscribe();
  }

  setMarker() {
    this.posList.forEach((pos) => {
      const marker = new naver.maps.Marker({
        position: new naver.maps.LatLng(pos.latitude, pos.longitude),
        map: this.map
      });

      naver.maps.Event.addListener(marker, 'click', () => {
        this.selectedPos = pos;
        this.map.setCenter(new naver.maps.LatLng(pos.latitude, pos.longitude));
      });

      this.markerList.push(marker);
    });
  }

  getPosList() {
    this.storage.get("posList").then(value => {
      if(!value) this.posList = [];
      else {
        this.posList = value;
        this.selectedPos = this.posList[0]; // set root pos
      }

      this.setMarker();
    });
  }

  getPosition() {
    if(this.bPositionLoading) return;
    this.bPositionLoading = true;
    console.log("positioning...");

    this.geolocation.getCurrentPosition({
      timeout: 10000
    }).then((resp) => {
      this.map.setCenter(new naver.maps.LatLng(resp.coords.latitude, resp.coords.longitude));
      this.bPositionLoading = false;
      console.log("find position");
    }).catch((error) => {
      console.log("Error get geolocation", error);
      this.bPositionLoading = false;

      let toast = this.toastCtrl.create({
        message: "GPS정보를 가져오는데 실패했습니다.",
        duration: 3000,
        position: 'top'
      });

      toast.present();
    })
  }

  votePos() {
    this.selectedPos.likeCount += 1;
  }

  openDetail() {
    this.detailPage.select(this.selectedPos);
    this.open = true;

    this.platform.ready().then(() => {
      if(this.platform.is('android')) {
        this.platform.registerBackButtonAction(() => {
          this.closeDetail(null);
        });
      }
    });
  }

  closeDetail($event) {
    this.open = false;

    this.platform.ready().then(() => {
      return;
    });
  }
}
