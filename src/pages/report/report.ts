import {Component, ElementRef, ViewChild} from '@angular/core';
import { NavController, NavParams, Platform, ToastController } from 'ionic-angular';
import { Geolocation } from "@ionic-native/geolocation";
import { Storage } from "@ionic/storage";

import { UserInput } from "../../models/userInput";


/**
 * Generated class for the ReportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var naver: any; // for naver map application

@Component({
  selector: 'page-report',
  templateUrl: 'report.html',
})
export class ReportPage {
  inputValues: UserInput = {};

  @ViewChild('reportMap') mapEle: ElementRef;
  defaultMapSize: {width: number, height: number};
  map: any;

  prevCoords: {lat: number, lng: number};
  currentPin: any;

  mapZoom: boolean = false;
  isLoading: boolean = true;

  bPositionLoading: boolean = false;  // 현재 위치 버튼 애니메이션

  fileName: string = "";

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public geolocation: Geolocation,
              public platform: Platform,
              public toastCtrl: ToastController,
              public storage: Storage) {
  }

  ionViewWillEnter() {
    this.defaultMapSize = {
      width: this.mapEle.nativeElement.offsetWidth,
      height: this.mapEle.nativeElement.offsetHeight
    };

    this.map = new naver.maps.Map('reportMap', {
      draggable: false,
      pinchZoom: false
    });


    this.currentPin = new naver.maps.Marker({
      position: new naver.maps.LatLng(37.5606715,126.995442),
      map: this.map
    });

    naver.maps.Event.addListener(this.map, 'drag', pointerEvent => {
      this.movePinToCenter();
    });

    this.geolocation.getCurrentPosition({
      timeout: 5000
    }).then((resp) => {
      this.map.setCenter(new naver.maps.LatLng(resp.coords.latitude, resp.coords.longitude));

      this.currentPin.setPosition(new naver.maps.LatLng(resp.coords.latitude, resp.coords.longitude));
      this.setAddress(this.currentPin.getPosition());

      this.isLoading = false;
    }).catch(error => {
      console.log("error occurred", error);
      this.map.setCenter(new naver.maps.LatLng(37.5606715,126.9954462));
      this.showToast("GPS정보를 가져오는데 실패했습니다.");

      this.isLoading = false;
    });
  }

  clickMap() {
    if(this.mapZoom) return;
    this.mapZoom = true;

    if(this.currentPin !== undefined) {
      this.map.setCenter(this.currentPin.getPosition());

      this.prevCoords = this.currentPin.getPosition();
    }

    this.map.setSize({
      width: this.platform.width(),
      height: this.platform.height() - 50
    });

    this.map.setOptions('draggable', true);

    this.platform.ready().then(() => {
      if(this.platform.is('android')) {
        this.platform.registerBackButtonAction(() => {
          this.setPrevPin();

          this.platform.registerBackButtonAction(() => {
            return;
          });
        });
      }
    });
  }

  setPin(event) {
    event.stopPropagation();

    if(!this.mapZoom) return;

    this.mapZoom = false;
    this.bPositionLoading = false;

    this.map.setSize(this.defaultMapSize);

    this.map.setOptions('draggable', false);
    this.map.setOptions('pinchZoom', false);


    this.setAddress(this.currentPin.getPosition());
  }

  setPrevPin() {
    this.mapZoom = false;
    this.bPositionLoading = false;

    this.map.setSize(this.defaultMapSize);

    this.map.setOptions('draggable', false);
    this.map.setOptions('pinchZoom', false);

    this.map.setCenter(this.prevCoords);
  }

  getPosition() {
    if(this.bPositionLoading) return;
    this.bPositionLoading = true;
    console.log("positioning...");

    this.geolocation.getCurrentPosition({
      timeout: 3000
    }).then((position) => {
      if(this.mapZoom) {
        this.map.setCenter(new naver.maps.LatLng(position.coords.latitude, position.coords.longitude));
        this.movePinToCenter();
        this.bPositionLoading = false;
      }
      console.log("find position");
    }).catch((error) => {
      console.log("Error get geolocation", error);
      this.showToast("GPS정보를 가져오는데 실패했습니다.");
      this.bPositionLoading = false;
    })
  }

  selectCategory(value: string) {
    this.inputValues.category = value;
  }

  inputContents(value: string) {
    this.inputValues.contents = value;
  }

  inputImage(event: any) {
    const reader = new FileReader();

    reader.readAsDataURL(event.srcElement.files[0]);
    this.fileName = event.srcElement.files[0].name;

    reader.onload = () => {
      this.inputValues.image = reader.result;
    };

    reader.onerror = (error) => {
      console.log('Error: ', error);
      this.showToast("이미지를 등록하는데 실패했습니다.");
    };

  }

  report() {
    console.log(this.inputValues);
    let message = "";

    if(this.inputValues.latitude === undefined || this.inputValues.longitude === undefined) {
      if(this.isLoading) {
        message = "GPS 정보를 가져오는 중입니다.";
      }
      else {
        message = "GPS 정보에 오류가 났습니다.";
      }
      this.showToast(message);
      return;
    }

    if(this.inputValues.category === undefined) {
      message = "카테고리를 입력해주세요.";
      this.showToast(message);
      return;
    }


    if(this.inputValues.image === undefined) {
      message = "이미지를 등록해주세요.";
      this.showToast(message);
      return;
    }

    alert("접수되었습니다.");

    this.storage.get("posList").then( value => {
      value.push({
        'category': this.inputValues.category,
        'contents':this.inputValues.contents,
        'latitude': this.inputValues.latitude,
        'longitude': this.inputValues.longitude,
        'address': this.inputValues.address,
        'likeCount': 0,
        'image': this.inputValues.image
      });
      this.storage.set("posList", value);
    });

    this.navCtrl.goToRoot({});
  }

  showToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'top'
    });

    toast.present();
  }

  setAddress(latlng: any) {
    const tm128 = naver.maps.TransCoord.fromLatLngToTM128(latlng);

    naver.maps.Service.reverseGeocode({
      location: tm128,
      coordType: naver.maps.Service.CoordType.TM128
    }, (status, response) => {
      if (status === naver.maps.Service.Status.ERROR) {
        this.showToast("주소를 가져오는데 문제가 생겼습니다.");
        return;
      }

      const items = response.result.items;

      this.inputValues.address = items[0].address;
    });
  }

  movePinToCenter() {
    const center = this.map.getCenter();
    console.log(center);

    this.currentPin.setPosition(center);
    this.inputValues.latitude = center._lat;
    this.inputValues.longitude = center._lng;
  }
}
