import { NgModule } from '@angular/core';
import { LoadingComponent } from './loading/loading';
@NgModule({
	declarations: [LoadingComponent],
	imports: [],
	exports: [LoadingComponent]
})
export class ComponentsModule {}
