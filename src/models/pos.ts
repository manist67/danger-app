export interface Pos {
  seq: number;
  latitude: number;
  longitude: number;
  name?: string;
  category?: string;
  likeCount?: number;
  address?: string;
  contents?: string;
  image?: string;
}
