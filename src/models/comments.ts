export interface Comments {
  id: string;
  contents: string;
  date: Date;
}
