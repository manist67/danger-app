export interface UserInput {
  category?: string;
  contents?: string;
  latitude?: number;
  longitude?: number;
  address?: string;
  image?: string;
}
